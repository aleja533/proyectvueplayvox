var estatusFiltro = new Vue()

Vue.component('tabla',{
  props:{
    status:{
      type: String,
      required: true
    }
  },
  template: `
  <div class="col-md-11 center">
    <div class="row total-items">
      <div class="m-3 text-bold"><p>{{ datos.length }} Requests</p></div>
    </div>
    <div class="row filtro-selector pt-4 pb-4">
      <selector @setstatus="setStatus"></selector>
    </div>
    <div class="row tabla-lista mt-5">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-12">
            <div class="row border-botom">
              <div class="col-md-2" v-for="(theader, index) in theaders">
                <h3 class="titulo-tabla">{{ theader }}</h3>
              </div>
            </div>
            <div class="row border-bottom p-3" v-for="(dato, index) in filtroEstatus">
              <div class="col-md-2" v-for="(item, jindex) in dato">
                <ul v-if="Array.isArray(item)">
                  <li v-for="list in item">
                    <span>{{ list }}</span>
                  </li>
                </ul>
                <span v-else>
                  <i v-if="jindex === 'priority'" class="fas fa-circle"
                    :class="{ high:item === 'high', medium:item === 'medium',low:item === 'low' }" ></i>
                  {{ item }}</span>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  `,
  data(){
    return{
      theaders: ['Status', 'Subject', 'Body', 'Priority', 'Csat', 'Tag', 'Metrics'],
      datos:[
        {
           'status': "solved",
           'subject': "No puedo ingresar a mi cuenta",
           'body': "Hola necesito ayuda para recuperar la contraseña de mi cuenta, ¿me pueden ayudar?",
           'priority': "high",
           'csat': 100,
           'tags': [ "account", "reset_password"],
           'metrics': [
               {
                   'id': "first_resolution_time_in_minutes",
                   'value': 30
               },
               {
                   'id': "first_reply_time_in_minutes",
                   'value': 10
               },
                {
                   'id': "full_resolution_time_in_minutes",
                   'value': 200
               }
           ]
        },
        {
           'status': "solved",
           'subject': "No puedo ingresar a mi cuenta",
           'body': "Hola necesito ayuda para recuperar la contraseña de mi cuenta, ¿me pueden ayudar?",
           'priority': "medium",
           'csat': 100,
           'tags': [ "account", "reset_password"],
           'metrics': [
               {
                   'id': "first_resolution_time_in_minutes",
                   'value': 30
               },
               {
                   'id': "first_reply_time_in_minutes",
                   'value': 10
               },
                {
                   'id': "full_resolution_time_in_minutes",
                   'value': 200
               }
           ]
        },
        {
           'status': "pending",
           'subject': "No puedo ingresar a mi cuenta",
           'body': "Hola necesito ayuda para recuperar la contraseña de mi cuenta, ¿me pueden ayudar?",
           'priority': "low",
           'csat': 100,
           'tags': [ "account", "reset_password"],
           'metrics': [
               {
                   'id': "first_resolution_time_in_minutes",
                   'value': 30
               },
               {
                   'id': "first_reply_time_in_minutes",
                   'value': 10
               },
                {
                   'id': "full_resolution_time_in_minutes",
                   'value': 200
               }
           ]
        }
      ],
    }
  },
  methods:{
    setStatus(value) {
        this.status = value;
    }
  },
  computed:{
    filtroEstatus() {
      if (this.status == 'All')
        {
          return this.datos;
        }
        else
        return this.datos.filter( (dato) => dato.status.includes(this.status) );
      }
  },

})

Vue.component('selector',{
  template: `
  <div class="col-md-12">
    <select v-model="status" class="button btn-gray2">
      <option class="dropdown-item"
        v-on:click="estatusSel"
        v-for="option in options"
        :value="option._id">
          {{ option.name }}</option>
    </select>
    <button class="btn-green">
      <a class="nav-link " href=""> <i class="verde fas fa-plus-circle"></i> Add Filter</a>
    </button>
  </div>
  `,

  data() {
    return {
      options: [
        {'_id': "All",'name': "All"},
        {'_id': "open",'name': "Open"},
        {'_id': "pending",'name': "Pending"},
        {'_id': "solved",'name': "Solved"},
        {'_id': "closed",'name': "Closed"},
        {'_id': "hold",'name': "Hold"},
      ],
      status: 'All',
    }
  },
  methods: {
    estatusSel(){
      this.$emit('setstatus', this.status);
    }
  }
})

var app = new Vue({
    el: '#app',
    data: {
      status:'All',
    },
    methods:{

    }
})
