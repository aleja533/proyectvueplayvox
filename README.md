# Plantilla para maquetacion en Bootstrap

_Plantilla base para realizar maquetas y prototipos alta fidelidad basadas en Bootstrap.

Template basado en [Start Bootstrap - Modern Business](https://startbootstrap.com/template-overviews/modern-business/)

Se utiliza con fines de prototipado de alta fidelidad y de forma académica.  

## Comenzando 🚀

### Pre-requisitos 📋

 Carpetas Vendor con Bootstrap y jQuery
 Para fines de pruebas tambien se agrego Vue.js


## Autores ✒️
* **Alejandra Zerdá ** - *Desarrolladora Front End*

## Licencia 📄

Este proyecto está bajo la Licencia GPL -
License: GNU General Public License
